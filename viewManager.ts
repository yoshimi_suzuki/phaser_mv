﻿class ViewManager {
    private static internalInstance: ViewManager = null;
    public static get instance() {
        if (ViewManager.internalInstance == null)
            ViewManager.internalInstance = new ViewManager();

        return ViewManager.internalInstance;
    }
    constructor() {
        // assert a constructor as private
        if (ViewManager.internalInstance != null)
            console.error("The constructor of ViewManager was called by outside. Do not new() a Singleton Class directly.");
    }


    private _views: Array<View> = new Array<View>();
    private _unregisterViews: Array<View> = new Array<View>();

    public register(view: View) {
        this._views.push(view);
    }

    public unregister(view: View) {
        this._unregisterViews.push(view);
    }

    public clear() {
        for (let view of this._views) {
            view.detach();
        }
        this._views = new Array<View>();
        this._unregisterViews = new Array<View>();
    }

    public tick() {
        if (this._unregisterViews.length > 0) {
            for (let view of this._unregisterViews) {
                this._views.splice(this._views.indexOf(view), 1);
            }
            this._unregisterViews = new Array<View>();
        }

        for (let view of this._views) {
            view.tick();
        }
    }
}