﻿class ImageComposer {
    constructor(
        private width: number,
        private height: number) {

    }

    private images: Array<ImageComposerElement> = new Array<ImageComposerElement>();
    private texts: Array<ImageComposerText> = new Array<ImageComposerText>();
    private atlases: Array<ImageComposerAtlas> = new Array<ImageComposerAtlas>();
    private imageFiles: Array<ImageComposerResource> = new Array<ImageComposerResource>();
    
    public addImage(image: ImageComposerElement) {
        this.images.push(image);
        return this;
    }
    public addText(text: ImageComposerText) {
        this.texts.push(text);
        return this;
    }
    public addAtlas(atlas: ImageComposerAtlas) {
        this.atlases.push(atlas);
        return this;
    }
    public addImageFile(imageFile: ImageComposerResource) {
        this.imageFiles.push(imageFile);
        return this;
    }

    public getBase64(callback: (base64: string) => void) {
        var flg: boolean = false;

        let game:Phaser.Game = null;
        game = new Phaser.Game(
            this.width,
            this.height,
            Phaser.CANVAS,
            'sub',
            {
                preload: () => {
                    game.preserveDrawingBuffer = true;

                    for(let atlas of this.atlases) {
                        game.load.atlasJSONHash(
                            atlas.tag,
                            "res/images/" + atlas.path + ".png",
                            "res/images/" + atlas.path + ".json"
                        );
                    }

                    for (let imageFile of this.imageFiles) {
                        game.load.image(
                            imageFile.tag,
                            "res/images/" + imageFile.path
                        );
                    }
                },
                create: () => {
                    for (let imageLayout of this.images) {
                        let image = ResourceManager.instance.isTextureAtlasName(imageLayout.tag)
                            ? game.add.image(
                                imageLayout.x,
                                imageLayout.y,
                                ResourceManager.instance.getTextureAtlasName(imageLayout.tag),
                                imageLayout.tag)
                            : game.add.image(
                                imageLayout.x,
                                imageLayout.y,
                                imageLayout.tag)
                        image.scale.x = imageLayout.scaleX;
                        image.scale.y = imageLayout.scaleY;
                        image.anchor.x = imageLayout.anchorX;
                        image.anchor.y = imageLayout.anchorY;
                    }
                    for(let textLayout of this.texts) {
                        let text = game.add.text(
                            textLayout.x,
                            textLayout.y,
                            textLayout.text,
                            {
                                font: "bold " + textLayout.size + "px Arial",
                                fill: textLayout.color,
                                boundsAlignH: "center",
                                boundsAlignV: "middle"                
                            }
                        );
                        text.anchor.x = textLayout.anchorX;
                        text.anchor.y = textLayout.anchorY;
                    }
                },
                update: () => { },
                render: () => {
                    if (flg) {
                        game.destroy();
                        return;
                    }
                    flg = true;
                    callback(game.canvas.toDataURL("image/png"));
                }
            });
    }
}
    
class ImageComposerElement {
    constructor(
        public tag: string,
        public x: number,
        public y: number,
        public scaleX: number = 1,
        public scaleY: number = 1,
        public rotation: number = 0,
        public anchorX: number = 0.5,
        public anchorY: number = 0.5) {
    }
}

class ImageComposerText {
    constructor(
        public text: string,
        public x: number,
        public y: number,
        public size: number = 24,
        public color: string = "#fff",
        public anchorX: number = 0.5,
        public anchorY: number = 0.5) {
    }
}

class ImageComposerAtlas {
    constructor(
        public tag: string,
        public path: string) {
    }
}

class ImageComposerResource {
    constructor(
        public tag: string,
        public path: string) {
    }
}