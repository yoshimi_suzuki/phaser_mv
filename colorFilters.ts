﻿// グレースケール用フィルタ(image等のfiltersに[new GrayFilter]を代入してお使い下さい)
class GrayFilter extends PIXI.AbstractFilter {
    constructor() {
        let uniforms = new PhaserModelView.Uniforms();
        uniforms.gray = { type: '1f', value: 1.0 };

        let fragmentSrc = [
            "precision mediump float;",

            "varying vec2       vTextureCoord;",
            "varying vec4       vColor;",
            "uniform sampler2D  uSampler;",
            "uniform float      gray;",

            "void main(void) {",
            "gl_FragColor = texture2D(uSampler, vTextureCoord);",
            "gl_FragColor.rgb = mix(gl_FragColor.rgb, vec3(0.2126 * gl_FragColor.r + 0.7152 * gl_FragColor.g + 0.0722 * gl_FragColor.b), gray);",
            "}"
        ];
        super(fragmentSrc, uniforms);

        return this;
    }

}
namespace PhaserModelView {
    export class Uniforms {
        gray: any;
    }
}
