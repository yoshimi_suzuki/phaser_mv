﻿class Model {
    constructor(
        private _position: Atagoal.Position = new Atagoal.Position()) {
    }


    protected onPositionUpdatedEvent: Phaser.Signal = new Phaser.Signal();
    public set onPositionUpdated(callback: () => void) {
        this.onPositionUpdatedEvent.add(callback);
    }

    public set position(position: Atagoal.Position) {
        this._position = position;
        this.onPositionUpdatedEvent.dispatch();
    }
    public get position() {
        return this._position;
    }
}