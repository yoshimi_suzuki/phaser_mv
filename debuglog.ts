﻿/// <reference path="phaserEngine.ts" />

class DebugLog {
    private static internalInstance: DebugLog = null;
    public static get instance() {

        if (DebugLog.internalInstance == null)
            DebugLog.internalInstance = new DebugLog();

        return DebugLog.internalInstance;
    }
    constructor() {
        // assert a constructor as private
        if (DebugLog.internalInstance != null)
            console.error("The constructor of DebugLog was called by outside. Do not new() a Singleton Class directly.");
    }

    private LineHeight: number = 15;
    private line: number = 1;
    private textColor: string = "#fff";
    private isDebug: boolean = false;

    public setDebugMode() {
        this.isDebug = true;
    }

    public setColor(color: string) {
        this.textColor = color;
    }
    public show(message: string) {
        if (!this.isDebug)
            return;
        this.line = 1;
        let length: number = message.length;
        let numberOfCharacterPerLine: number = 40;
        let textPos: number = 0;
        while (textPos < length) {
            let lineMessage = message.substring(
                textPos,
                textPos + numberOfCharacterPerLine < length
                    ? textPos + numberOfCharacterPerLine
                    : length);
            PhaserEngine.game.debug.text(
                lineMessage,
                0,
                this.line * this.LineHeight,
                this.textColor);
            this.line++;
            textPos += numberOfCharacterPerLine;
        }
    }
    public clear() {
        PhaserEngine.game.debug.reset();
        this.line = 1;
        this.textColor = "#fff";
    }
    public static show(message: string) {
        DebugLog.instance.show(message);
    }
    public static clear() {
        DebugLog.instance.clear();
    }
}