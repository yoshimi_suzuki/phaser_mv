﻿class ResourceManager {
    private static internalInstance: ResourceManager = null;
    public static get instance() {

        if (ResourceManager.internalInstance == null)
            ResourceManager.internalInstance = new ResourceManager();

        return ResourceManager.internalInstance;
    }
    constructor() {
        // assert a constructor as private
        if (ResourceManager.internalInstance != null)
            console.error("The constructor of ResourceManager was called by outside. Do not new() a Singleton Class directly.");
    }


    public initialize(){
        PhaserEngine.game.load.onFileComplete.add((progress, key, success) => {
            this._onProgressUpdated.dispatch(progress);

            if (!success)
                return;


            this._onImageLoaded.dispatch(key);
        });
    }


    private _onImageLoaded: Phaser.Signal = new Phaser.Signal();
    public set onImageLoaded(callback:(loadedImageTag: string)=>void){
        this._onImageLoaded.add(callback);
    }

    private _onProgressUpdated: Phaser.Signal = new Phaser.Signal();
    public set onProgressUpdated(callback:(progress: number)=>void){
        this._onProgressUpdated.add(callback);
    }

    private imageTags: ImageTags = new ImageTags();
    private soundTags: SoundTags = new SoundTags();

    private totalPreloadFileCount: number;
    private preloadedFileCount: number;

    public prepareImage(tag: string, imagePath: string) {
        PhaserEngine.game.load.image(tag, imagePath);
    }

    public prepareTextureAtlas(
        tag: string,
        imagePath: string,
        jsonPath: string) {
        let loader = PhaserEngine.game.load.atlasJSONHash(tag, imagePath, jsonPath);     
    }

    public preload(onProgressUpdated: (progress: number) => void) {
        this.totalPreloadFileCount =
            this.imageTags.preloadImageTags.length +
        this.imageTags.preloadAtlasTags.length +
            this.soundTags.preloadSoundTags.length;
        this.preloadedFileCount = 0;

        this.prepareImages(onProgressUpdated);
        this.prepareSounds(onProgressUpdated);
    }

    public prepareImages(onProgressUpdated:(progress: number) => void) {
        let total: number =
            this.imageTags.preloadImageTags.length +
            this.imageTags.preloadAtlasTags.length;
        let loaded: number = 0;
        for (let tag of this.imageTags.preloadImageTags) {
            this.prepareImage(tag, this.imageTags.getImagePath(tag));
            loaded++;
            onProgressUpdated(100.0 * loaded / total);
        }
        for (let tag of this.imageTags.preloadAtlasTags) {
            this.prepareTextureAtlas(
                tag,
                this.imageTags.getAtlasPath(tag) + ".png",
                this.imageTags.getAtlasPath(tag) + ".json");
            loaded++;
            onProgressUpdated(100.0 * loaded / total);
        }
    }

    public prepareSounds(onProgressUpdated: (progress: number) => void) {
        for (let soundTag of this.soundTags.preloadSoundTags) {
            PhaserEngine.game.load.audio(soundTag, this.soundTags.getSoundPath(soundTag));
            this.preloadedFileCount++;
            onProgressUpdated(100.0 * this.preloadedFileCount / this.totalPreloadFileCount);
        }
    }

    public getTextureAtlasName(imageName: string): string {
        return this.imageTags.getTextureAtlasName(imageName);
    }

    public isTextureAtlasName(imageName: string): boolean {
        return this.imageTags.isTextureAtlasName(imageName);
    }

    public loadImage(
        tag: string,
        path: string,
        loader: Phaser.Loader = PhaserEngine.game.load) {
        let imagePath = path != DelayedLoadImage.InvalidPath
            ? path
            : this.imageTags.getImagePath(tag);
        if (imagePath == null)
            return;
        loader.image(tag, imagePath);
    }

    public loadTextureAtlas(
        tag: string,
        path: string,
        loader: Phaser.Loader = PhaserEngine.game.load) {
        let atlasPath = path != DelayedLoadAtlas.InvalidPath
            ? path
            : this.imageTags.getAtlasPath(tag);
        if (atlasPath == null)
            return;

        loader.atlasJSONHash(
            tag,
            atlasPath + ".png",
            atlasPath + ".json");
    }
}