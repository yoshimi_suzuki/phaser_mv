﻿namespace MV {
    export class ImageTags {
        public static get White() { return "white"; }

        public static get ButtonFrames() {
            let map = new KVSMap<string>();
            map.register(Image9SliceTags.LeftBottom, "button_frame_7.png");
            map.register(Image9SliceTags.Bottom, "button_frame_8.png");
            map.register(Image9SliceTags.RightBottom, "button_frame_9.png");
            map.register(Image9SliceTags.Left, "button_frame_4.png");
            map.register(Image9SliceTags.Center, "button_frame_5.png");
            map.register(Image9SliceTags.Right, "button_frame_6.png");
            map.register(Image9SliceTags.LeftTop, "button_frame_1.png");
            map.register(Image9SliceTags.Top, "button_frame_2.png");
            map.register(Image9SliceTags.RightTop, "button_frame_3.png");
            return map;
        }
    
        public static get DefaultExpandableFrames() {
            let map = new KVSMap<string>();
            map.register(Image9SliceTags.LeftBottom, "kari/frame1_1");
            map.register(Image9SliceTags.Bottom, "kari/frame1_2");
            map.register(Image9SliceTags.RightBottom, "kari/frame1_3");
            map.register(Image9SliceTags.Left, "kari/frame1_4");
            map.register(Image9SliceTags.Center, "kari/frame1_5");
            map.register(Image9SliceTags.Right, "kari/frame1_6");
            map.register(Image9SliceTags.LeftTop, "kari/frame1_7");
            map.register(Image9SliceTags.Top, "kari/frame1_8");
            map.register(Image9SliceTags.RightTop, "kari/frame1_9");
            return map;
        }
    }
}