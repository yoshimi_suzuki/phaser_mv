﻿class PhaserEngine {
    private static internalInstance: PhaserEngine = null;
    public static get instance() {

        if (PhaserEngine.internalInstance == null)
            PhaserEngine.internalInstance = new PhaserEngine();

        return PhaserEngine.internalInstance;
    }
    constructor() {
        // assert a constructor as private
        if (PhaserEngine.internalInstance != null)
            console.error("The constructor of PhaserEngine was called by outside. Do not new() a Singleton Class directly.");
    }

    private internalGame: Phaser.Game = null;
    public initialize(gameRef: Phaser.Game) {
        this.internalGame = gameRef;
    }

    public get game() {
        return this.internalGame;
    }
    public static get game() {
        return PhaserEngine.instance.game;
    }
}