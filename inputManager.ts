﻿class InputManager {
    private static internalInstance: InputManager = null;
    public static get instance() {
        if (InputManager.internalInstance == null)
            InputManager.internalInstance = new InputManager();

        return InputManager.internalInstance;
    }
    constructor() {
        // assert a constructor as private
        if (InputManager.internalInstance != null)
            console.error("The constructor of InputManager was called by outside. Do not new() a Singleton Class directly.");
    }

    public teardown() {
        PhaserEngine.game.input.onDown.removeAll();
        PhaserEngine.game.input.onUp.removeAll();
        PhaserEngine.game.input.onTap.removeAll();
        InputManager.internalInstance = null;
    }

    private _onTapped: Phaser.Signal = new Phaser.Signal();
    private _onSwiped: Phaser.Signal = new Phaser.Signal();

    private _onTouchBegan: Phaser.Signal = new Phaser.Signal();
    private _onTouchMoved: Phaser.Signal = new Phaser.Signal();
    private _onTouchFinished: Phaser.Signal = new Phaser.Signal();


    private touchStartPosition: Atagoal.Position = new Atagoal.Position();
    private touchHoldPosition: Atagoal.Position = null;
    private touchEndPosition: Atagoal.Position = new Atagoal.Position();


    public set onTapped(callback: (position: Atagoal.Position) => void) {
        this._onTapped.add(callback);
    }
    public set onSwiped(callback: (direction: Atagoal.Vector) => void) {
        this._onSwiped.add(callback);
    }

    public set onTouchBegan(callback: (position: Atagoal.Position) => void) {
        this._onTouchBegan.add(callback);
    }
    public set onTouchMoved(callback: (diff: Atagoal.Vector) => void) {
        this._onTouchMoved.add(callback);
    }
    public set onTouchFinished(callback: (
        position: Atagoal.Position,
        direction: Atagoal.Vector) => void) {
        this._onTouchFinished.add(callback);
    }

    public initialize() {
        // assert
        if (PhaserEngine.game == null)
            console.error("The PhaserEngine was not initialized yet.");

        PhaserEngine.game.input.onDown.add(() => {
            if (Pause.instance.paused)
                return;

            this.touchStartPosition.x = PhaserEngine.game.input.activePointer.x;
            this.touchStartPosition.y = PhaserEngine.game.input.activePointer.y;
            this.touchHoldPosition = this.touchStartPosition.clone();

            this._onTouchBegan.dispatch(this.touchStartPosition);
        });
        PhaserEngine.game.input.addMoveCallback(() => {
            if (Pause.instance.paused
                || this.touchHoldPosition == null
                || PhaserEngine.game.input.activePointer.isUp)
                return;

            let oldPosition = this.touchHoldPosition.clone();

            this.touchHoldPosition.x = PhaserEngine.game.input.activePointer.x;
            this.touchHoldPosition.y = PhaserEngine.game.input.activePointer.y;

            this._onTouchMoved.dispatch(Atagoal.Vector.createFromPoints(oldPosition, this.touchHoldPosition));
        }, "content");
        PhaserEngine.game.input.onUp.add(() => {
            if (Pause.instance.paused)
                return;

            this.touchHoldPosition = null;

            this.touchEndPosition.x = PhaserEngine.game.input.activePointer.x;
            this.touchEndPosition.y = PhaserEngine.game.input.activePointer.y;

            let direction = Atagoal.Vector.createFromPoints(this.touchStartPosition, this.touchEndPosition);
            this._onTouchFinished.dispatch(
                this.touchEndPosition,
                direction);

            let distance: number = direction.length;

            if (distance > 20) {
                this._onSwiped.dispatch(direction);
            }
        });
        PhaserEngine.game.input.onTap.add(() => {
            if (Pause.instance.paused)
                return;

            this._onTapped.dispatch(this.touchEndPosition);
        });
    }

    public clear() {
        this._onTapped = new Phaser.Signal();
        this._onSwiped = new Phaser.Signal();
        this._onTouchBegan = new Phaser.Signal();
        this._onTouchMoved = new Phaser.Signal();
        this._onTouchFinished = new Phaser.Signal();
    }
}