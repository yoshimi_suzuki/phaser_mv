﻿class DebugMode {
    private static internalInstance: DebugMode = null;
    public static get instance() {

        if (DebugMode.internalInstance == null)
        DebugMode.internalInstance = new DebugMode();

        return DebugMode.internalInstance;
    }
    constructor() {
        // assert a constructor as private
        if (DebugMode.internalInstance != null)
            console.error("The constructor of DebugMode has been called by outside. Do not new() a Singleton Class directly.");
    }


    public setActive() {
        this._isActive = true;
    }

    private _isActive:boolean = false;

    public get isActive() {
        return this._isActive;
    }
}