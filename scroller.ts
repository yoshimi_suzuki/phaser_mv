﻿class Scroller {
    private _models:Array<Model> = new Array<Model>();

    private _offset:Atagoal.Vector = new Atagoal.Vector();
    private _velocity:Atagoal.Vector = new Atagoal.Vector();
    private _friction:number = 1;

    private _isVerticalAvailable:boolean = true;
    private _isHorizontalAvailable:boolean = true;

    private _leftTop:Atagoal.Vector = new Atagoal.Vector(-100, -100);
    private _rightBottom:Atagoal.Vector = new Atagoal.Vector(100, 100);

    public tick() {
        this.applyVelocity();
        this.applyFriction();
    }

    private applyFriction() {
        let power = this._velocity.length;
        if (power == 0) return;
        let newPower = power - this._friction;
        if (newPower < 0) newPower = 0;
        this._velocity.scale(newPower / power);
    }

    private applyVelocity() {
        if (this._velocity.length == 0) return;
        
        let estimate = new Atagoal.Vector(
            this._offset.x + this._velocity.x,
            this._offset.y + this._velocity.y 
        );


        let limitOver = new Atagoal.Vector();
        if (estimate.x < this._leftTop.x) {
            limitOver.x = this._leftTop.x - estimate.x;
        }
        if (estimate.x > this._rightBottom.x) {
            limitOver.x = this._rightBottom.x - estimate.x;
        }
        if (estimate.y < this._leftTop.y) {
            limitOver.y = this._leftTop.y - estimate.y;
        }
        if (estimate.y > this._rightBottom.y) {
            limitOver.y = this._rightBottom.y - estimate.y;
        }
        this._velocity.move(limitOver);


        for(let model of this._models) {
            model.position = model.position.getMovedPosition(this._velocity);
        }
        this._offset.move(this._velocity);
        this._onOffsetUpdated.dispatch();
    }

    // ----------------------- setting up
    public setVertical(enabled:boolean) {
        this._isVerticalAvailable = enabled;
        return this;
    }

    public setHorizontal(enabled:boolean) {
        this._isHorizontalAvailable = enabled;
        return this;
    }

    public setLimit(
        leftTop:Atagoal.Vector,
        rightBottom:Atagoal.Vector
    ) {
        this._leftTop = leftTop;
        this._rightBottom = rightBottom;
        return this;
    }

    public setFriction(friction:number) {
        this._friction = friction;
        return this;
    }

    public addModel(model:Model) {
        if (this._models.indexOf(model) >= 0) return;
        this._models.push(model);
    }

    // ----------------------- methods
    public setVelocity(velocity:Atagoal.Vector) {
        this._velocity = velocity;
        if (!this._isHorizontalAvailable) {
            this._velocity.x = 0;
        }
        if (!this._isVerticalAvailable) {
            this._velocity.y = 0;
        }
    }
    
    public addVelocity(velocity:Atagoal.Vector) {
        this._velocity.move(velocity);
        if (!this._isHorizontalAvailable) {
            this._velocity.x = 0;
        }
        if (!this._isVerticalAvailable) {
            this._velocity.y = 0;
        }
    }

    public removeAll() {
        this._models = new Array<Model>();
        this._offset = new Atagoal.Vector();
        this._velocity = new Atagoal.Vector();
    }

    public reset() {
        for(let model of this._models) {
            model.position = model.position.getMovedPosition(this._offset.clone().scale(-1));
        }
        this._offset = new Atagoal.Vector();
        this._velocity = new Atagoal.Vector();
    }

    public moveImmediately(offset:Atagoal.Vector) {
        if (!this._isHorizontalAvailable) {
            offset.x = 0;
        }
        if (!this._isVerticalAvailable) {
            offset.y = 0;
        }
        let diff = new Atagoal.Vector(
            offset.x - this._offset.x,
            offset.y - this._offset.y
        );
        for(let model of this._models) {
            model.position = model.position.getMovedPosition(diff.clone());
        }
        this._offset = offset;
        this._velocity = new Atagoal.Vector();
    }

    // ----------------------- events
    private _onOffsetUpdated:EventHolder = new EventHolder();
    public set onOffsetUpdated(callback:()=>void) {
        this._onOffsetUpdated.add(callback);
    }

    // ------------------------ accessors
    public get offset() {
        return this._offset;
    }
}