﻿class View {
    constructor(
        private storedPosition: Atagoal.Position = new Atagoal.Position(),
        protected game: Phaser.Game = PhaserEngine.game) {
        this.node = this.game.add.sprite(this.resolvedPosition.x, this.resolvedPosition.y);
        ViewManager.instance.register(this);
    }

    public detach() {
        if (this.node == null)
            return;
        this.node.destroy();
        this.node = null;
        ViewManager.instance.unregister(this);

        for(let view of this.views) {
            view.detach();
        }
    }

    protected model: Model = null;

    protected node: Phaser.Sprite = null;
    protected image: Phaser.Image = null;
    protected imageSizes: KVSMap<Atagoal.Vector> = new KVSMap<Atagoal.Vector>();

    protected views:Array<View> = new Array<View>();

    public tick() {
    }

    public setModel(model: Model) {
        this.model = model;

        this.model.onPositionUpdated = () => {
            this.setPosition(this.model.position);
        }
    }

    public get position() {
        return this.storedPosition;
    }

    public setParent(parent: Phaser.Sprite) {
        if (this.node == null) return;
        parent.addChild(this.node);
    }

    public addChild(view:View) {
        if (this.node == null) return;
        view.setParent(this.node);
        this.views.push(view);
    }

    public setImage(
        imageName: string,
        offset: Atagoal.Position = new Atagoal.Position(0, 0)) {
        if (this.node == null) return;
        let resolvedOffset = ResolutionManager.instance.convertPosition(offset);
        this.image =
            ResourceManager.instance.isTextureAtlasName(imageName)
            ? this.game.add.image(
                    resolvedOffset.x, resolvedOffset.y,
                    ResourceManager.instance.getTextureAtlasName(imageName),
                    imageName)
            : this.game.add.image(
                    resolvedOffset.x, resolvedOffset.y,
                    imageName);
        this.imageSizes.register(imageName, new Atagoal.Vector(this.image.width,this.image.height));
        this.image.anchor.x = 0.5;
        this.image.anchor.y = 0.5;
        this.image.scale.x = ResolutionManager.instance.scale;
        this.image.scale.y = ResolutionManager.instance.scale;
        this.node.addChild(this.image);
    }

    public addImage(
        imageName: string,
        offset: Atagoal.Position = new Atagoal.Position(0, 0)) {
        if (this.node == null) return;
        let resolvedOffset = ResolutionManager.instance.convertPosition(offset);
        let image =
            ResourceManager.instance.isTextureAtlasName(imageName)
                ? this.game.add.image(
                    resolvedOffset.x, resolvedOffset.y,
                    ResourceManager.instance.getTextureAtlasName(imageName),
                    imageName)
                : this.game.add.image(
                    resolvedOffset.x, resolvedOffset.y,
                    imageName);
        this.imageSizes.register(imageName, new Atagoal.Vector(image.width, image.height));
        image.anchor.x = 0.5;
        image.anchor.y = 0.5;
        image.scale.x = ResolutionManager.instance.scale;
        image.scale.y = ResolutionManager.instance.scale;
        this.node.addChild(image);

        return image;
    }

    public getImageSize(
        imageName:string
    ) {
        return this.imageSizes.get(imageName);
    }

    public setGrayFilter() {
        this.setFilter(new GrayFilter());
    }

    protected setFilter(filter:PIXI.AbstractFilter) {
        if (this.node == null) return;
        for (let displayObject of this.node.children) {
            displayObject.filters = [filter];
        }
    }
    
    protected addText(
        message: string,
        position: Atagoal.Position,
        size: number = 16,
        color: string = "#fff"
    ) {
        if (this.node == null) return;
        let fontSize = size * ResolutionManager.instance.scale;
        let text = PhaserEngine.game.add.text(
            position.x * ResolutionManager.instance.scale,
            position.y * ResolutionManager.instance.scale,
            message,
            {
                font: "bold " + fontSize + "px Arial",
                fill: color,
                boundsAlignH: "center",
                boundsAlignV: "middle"
            });
            
        this.node.addChild(text);

        return text;
    }

    protected addSquareMaskNode(
        leftTop: Atagoal.Position,
        rightBottom: Atagoal.Position
    ) {
        if (this.node == null) return;
        leftTop = ResolutionManager.instance.convertPosition(leftTop);
        rightBottom = ResolutionManager.instance.convertPosition(rightBottom);
        let centerPosition = new Atagoal.Position(
            (leftTop.x + rightBottom.x) / 2,
            (leftTop.y + rightBottom.y) / 2
        );

        let maskNode = PhaserEngine.game.add.graphics(
            centerPosition.x,
            centerPosition.y
        );

        let getPolygon = ()=>{
            let vertice = new Array<Atagoal.Position>();

            vertice.push(
                new Atagoal.Position(
                    leftTop.x - centerPosition.x,
                    leftTop.y - centerPosition.y
                )
            );
            vertice.push(
                new Atagoal.Position(
                    rightBottom.x - centerPosition.x,
                    leftTop.y - centerPosition.y
                )
            );
            vertice.push(
                new Atagoal.Position(
                    rightBottom.x - centerPosition.x,
                    rightBottom.y - centerPosition.y
                )
            );
            vertice.push(
                new Atagoal.Position(
                    leftTop.x - centerPosition.x,
                    rightBottom.y - centerPosition.y
                )
            );


            let points = new Array<Phaser.Point>();
            for (let i=0; i<vertice.length; i++){
                points.push(new Phaser.Point(
                    vertice[i].x * ResolutionManager.instance.scale,
                    vertice[i].y * ResolutionManager.instance.scale
                ));
            }
            points.push(new Phaser.Point(
                vertice[0].x * ResolutionManager.instance.scale,
                vertice[0].y * ResolutionManager.instance.scale
            ));

            return points;
        };

        maskNode.beginFill(0xffffff);
        maskNode.drawPolygon(getPolygon());
        maskNode.endFill();
        maskNode.isMask = true;
        this.node.addChild(maskNode);

        return maskNode;
    }

    protected setPosition(position: Atagoal.Position) {
        if (this.node == null) return;
        this.storedPosition = position;
        if (this.node != null) {
            this.node.x = this.resolvedPosition.x;
            this.node.y = this.resolvedPosition.y;
        }
    }

    protected switchImage(imageName: string) {
        if (this.node == null) return;
        if (ResourceManager.instance.isTextureAtlasName(imageName)) {
            this.image.frameName = imageName;
        } else {
            this.image.destroy();
            this.setImage(imageName);
        }
    }

    protected setScaleX(scale: number) {
        if (this.node == null) return;
        this.node.scale.x = scale * ResolutionManager.instance.scale;
    }
    protected setScaleY(scale: number) {
        if (this.node == null) return;
        this.node.scale.y = scale * ResolutionManager.instance.scale;
    }

    private get resolvedPosition() {
        return ResolutionManager.instance.convertPosition(this.storedPosition);
    }
}