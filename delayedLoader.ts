﻿class DelayedLoadManager {
    private static internalInstance: DelayedLoadManager = null;
    public static get instance() {

        if (DelayedLoadManager.internalInstance == null)
            DelayedLoadManager.internalInstance = new DelayedLoadManager();

        return DelayedLoadManager.internalInstance;
    }
    constructor() {
        // assert a constructor as private
        if (DelayedLoadManager.internalInstance != null)
            console.error("The constructor of DelayedLoadManager was called by outside. Do not new() a Singleton Class directly.");
    }

    private imageLoaders: Array<DelayedImageLoader> = new Array<DelayedImageLoader>();
    private atlasLoaders: Array<DelayedAtlasLoader> = new Array<DelayedAtlasLoader>();
    private jsonLoaders: Array<DelayedJsonLoader> = new Array<DelayedJsonLoader>();

    public registerImages(
        images: Array<DelayedLoadImage>
    ) {
        let loader = new DelayedImageLoader();
        loader.initialize();
        for (let image of images)
            loader.queue(
                image.tag,
                image.path
            );
        loader.flush();

        this.imageLoaders.push(loader);
    }

    public registerAtlases(
        atlases: Array<DelayedLoadAtlas>
    ) {
        let loader = new DelayedAtlasLoader();
        loader.initialize();
        for (let atlas of atlases)
            loader.queue(
                atlas.tag,
                atlas.path
            );
        loader.flush();

        this.atlasLoaders.push(loader);
    }

    public registerJsons(
        jsons: Array<DelayedLoadJson>
    ) {
        let loader = new DelayedJsonLoader();
        loader.initialize();
        for (let json of jsons)
            loader.queue(
                json.tag,
                json.path
            );
        loader.flush();

        this.jsonLoaders.push(loader);
    }

    public getImage(
        imageTag: string,
        callback: () => void) {
        for (let loader of this.imageLoaders) {
            if (!loader.isQueued(imageTag))
                continue;
            loader.get(imageTag, callback);
            return;
        }
    }

    public getAtlas(
        atlasTag: string,
        callback: () => void) {
        for (let loader of this.atlasLoaders) {
            if (!loader.isQueued(atlasTag))
                continue;
            loader.get(atlasTag, callback);
            return;
        }
    }
    
    public getJson(
        jsonTag: string,
        callback: () => void) {
        for (let loader of this.jsonLoaders) {
            if (!loader.isQueued(jsonTag))
                continue;
            loader.get(jsonTag, callback);
            return;
        }
    }

    public isAtlasLoaded(atlasTag: string) {
        for (let loader of this.atlasLoaders) {
            if (loader.isLoaded(atlasTag))
                return true;
            continue;
        }
        return false;
    }
    
    public isJsonLoaded(jsonTag: string) {
        for (let loader of this.jsonLoaders) {
            if (loader.isLoaded(jsonTag))
                return true;
            continue;
        }
        return false;
    }
}

class DelayedLoadImage {
    public static InvalidPath = "xxx";
    constructor(
        public tag: string,
        public path: string = DelayedLoadImage.InvalidPath
    ) { }
}

class DelayedLoadAtlas {
    public static InvalidPath = "xxx";
    constructor(
        public tag: string,
        public path: string = DelayedLoadAtlas.InvalidPath
    ) { }
}

class DelayedLoadJson {
    public static InvalidPath = "xxx";
    constructor(
        public tag: string,
        public path: string = DelayedLoadJson.InvalidPath
    ) { }
}

class DelayedImageLoader {
    public initialize() {
        this._loader = new Phaser.Loader(PhaserEngine.game);

        this._loader.onFileComplete.add((progress, key, success) => {
            let callback = this._callbacks.get(key);
            if (callback != null)
            callback();
            this._loadedTags.push(key);
        });
    }

    private _queuedTags: Array<string> = new Array<string>();
    private _loadedTags: Array<string> = new Array<string>();

    private _callbacks: KVSMap<() => void> = new KVSMap<() => void>();

    private _loader: Phaser.Loader = null;

    public queue(
        imageTag: string,
        path: string = "xxx") {
        if (this.isQueued(imageTag)
            || this.isLoaded(imageTag))
            return;

        this._queuedTags.push(imageTag);
        ResourceManager.instance.loadImage(imageTag, path, this._loader);
    }
   
    public flush() {
        this._loader.start();
    }

    public get(
        imageTag:string,
        callback:() => void) {
        if (this.isLoaded(imageTag)){
            callback();
            return;
        }

        this._callbacks.register(imageTag, callback);
    }

    public isQueued(imageTag: string) {
        return this._queuedTags.indexOf(imageTag) >= 0;
    }

    public isLoaded(imageTag: string) {
        return this._loadedTags.indexOf(imageTag) >= 0;
    }
}

class DelayedAtlasLoader {
    public initialize() {
        this._loader = new Phaser.Loader(PhaserEngine.game);

        this._loader.onFileComplete.add((progress, key, success) => {
            let callback = this._callbacks.get(key);
            if (callback != null)
                callback();
            this._loadedTags.push(key);
        });
    }

    private _queuedTags: Array<string> = new Array<string>();
    private _loadedTags: Array<string> = new Array<string>();

    private _callbacks: KVSMap<() => void> = new KVSMap<() => void>();

    private _loader: Phaser.Loader = null;

    public queue(
        atlasTag: string,
        path: string = "xxx") {
        if (this.isQueued(atlasTag)
            || this.isLoaded(atlasTag))
            return;

        this._queuedTags.push(atlasTag);
        ResourceManager.instance.loadTextureAtlas(atlasTag, path, this._loader);
    }

    public flush() {
        this._loader.start();
    }

    public get(
        atlasTag: string,
        callback: () => void) {
        if (this.isLoaded(atlasTag)) {
            callback();
            return;
        }

        this._callbacks.register(atlasTag, callback);
    }

    public isQueued(atlasTag: string) {
        return this._queuedTags.indexOf(atlasTag) >= 0;
    }

    public isLoaded(atlasTag: string) {
        return this._loadedTags.indexOf(atlasTag) >= 0;
    }
}

class DelayedJsonLoader {
    public initialize() {
        this._loader = new Phaser.Loader(PhaserEngine.game);

        this._loader.onFileComplete.add((progress, key, success) => {
            let callback = this._callbacks.get(key);
            if (callback != null)
                callback();
            this._loadedTags.push(key);
        });
    }

    private _queuedTags: Array<string> = new Array<string>();
    private _loadedTags: Array<string> = new Array<string>();

    private _callbacks: KVSMap<() => void> = new KVSMap<() => void>();

    private _loader: Phaser.Loader = null;

    public queue(
        jsonTag: string,
        path: string = "xxx") {
        if (this.isQueued(jsonTag)
            || this.isLoaded(jsonTag))
            return;

        this._queuedTags.push(jsonTag);
        
        this._loader.json(jsonTag, "res/data/" + path);
    }

    public flush() {
        this._loader.start();
    }

    public get(
        jsonTag: string,
        callback: () => void) {
        if (this.isLoaded(jsonTag)) {
            callback();
            return;
        }

        this._callbacks.register(jsonTag, callback);
    }

    public isQueued(jsonTag: string) {
        return this._queuedTags.indexOf(jsonTag) >= 0;
    }

    public isLoaded(jsonTag: string) {
        return this._loadedTags.indexOf(jsonTag) >= 0;
    }
}