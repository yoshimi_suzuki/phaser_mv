﻿class Sequence {
    protected game: Phaser.Game;
    constructor() {
        this.game = PhaserEngine.instance.game;
    }

    public onFinished: (tag: string) => void;


    public initialize() {
    }

    public tick() {
        ViewManager.instance.tick();
    }

    public end(tag: string = "") {
        ViewManager.instance.clear();
        this.onFinished(tag);
    }
}