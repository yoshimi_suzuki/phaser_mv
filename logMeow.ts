﻿class LogMeow {
    private static internalInstance: LogMeow = null;
    public static get instance() {

        if (LogMeow.internalInstance == null)
        LogMeow.internalInstance = new LogMeow();

        return LogMeow.internalInstance;
    }
    constructor() {
        // assert a constructor as private
        if (LogMeow.internalInstance != null)
            console.error("The constructor of LogMeow has been called by outside. Do not new() a Singleton Class directly.");
    }

    private _message:string = "";
    private _view:LogMeowView = null;

    public show(message:string) {
        this._message = message;

        if (this._view != null) this._view.detach();
        this._view = LogMeowView.attach().show(this._message);
    }

    public add(message:string) {
        this._message += "\n" + message;

        if (this._view != null) this._view.detach();
        this._view = LogMeowView.attach().show(this._message);
    }

    public hide() {
        if (this._view != null) this._view.detach();
    }
}