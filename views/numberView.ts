﻿class NumberView extends View {
    public static attach(
        value: number,
        position: Atagoal.Position,
        imageNames: Array<string>) {
        let view: NumberView = new NumberView(
            value,
            position,
            imageNames);

        ViewManager.instance.register(view);
        return view;
    }

    constructor(
        value: number,
        position: Atagoal.Position,
        private imageNames: Array<string>) {
        super();

        this.setImage(this.imageNames[value]);
        this.setPosition(position);
    }

    public detach() {
        super.detach();
        ViewManager.instance.unregister(this);
    }

    public setNumber(value: number){
        if (value < 0)
            return;
        if (value > 9)
            return;

        this.switchImage(this.imageNames[value]);
    }

    public setComma(){
        this.switchImage(this.imageNames[10]);
    }

    public hide() {
        this.image.alpha = 0;
    }

    public show() {
        this.image.alpha = 1;
    }

    public set tint(value: number) {
        this.image.tint = value;
    }

    public updatePosition(position: Atagoal.Position) {
        this.setPosition(position);
    }
}