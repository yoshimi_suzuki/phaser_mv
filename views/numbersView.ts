﻿class NumbersView extends View {
    public static attach() {
        let view: NumbersView = new NumbersView();

        ViewManager.instance.register(view);
        return view;
    }

    public detach() {
        for (let view of this.numbers)
            view.detach();
        super.detach();

        ViewManager.instance.unregister(this);
    }



    private numbers: Array<NumberView> = new Array<NumberView>();

    private MaxDigit: number = 3;
    private NumberWidth: number = 20;
    private BasePosition: Atagoal.Position = new Atagoal.Position(0, 0);
    private Offset: Atagoal.Position = new Atagoal.Position(0, 0);
    private isCentering:boolean = false;

    public setup(
        basePosition: Atagoal.Position,
        numberImageSet: Array<string>,
        maxDigit: number = 6,
        numberWidth: number = 20,
        offset: Atagoal.Position = new Atagoal.Position(0, 0),
        isCentering: boolean = false
    ): NumbersView {
        this.MaxDigit = maxDigit;
        this.NumberWidth = numberWidth;
        this.BasePosition = basePosition.clone();
        this.Offset = offset;
        this.isCentering = isCentering;

        for (let i = 0; i < this.MaxDigit; i++) {
            let position = new Atagoal.Position(
                basePosition.x + this.Offset.x + -1 * (this.NumberWidth * i),
                basePosition.y + this.Offset.y
            );
            
            if (this.isCentering) {
                position.x += this.getWidth(0) / 2;
            }

            let view = NumberView.attach(
                0,
                position,
                numberImageSet
            );
            view.hide();
            view.setParent(this.node);

            this.numbers.push(view)
        }

        return this;
    }

    public updateNumbers(
        value: number,
        newOffset: Atagoal.Position = this.Offset) {
        this.Offset = newOffset;
        this.clearNumbers();

        for (let i = 0; i < this.MaxDigit; i++) {
            let position = new Atagoal.Position(
                this.BasePosition.x + this.Offset.x + -1 * (this.NumberWidth * i),
                this.BasePosition.y + this.Offset.y
            );

            if (this.isCentering) {
                 position.x += this.getWidth(value) / 2;
            }

            this.numbers[i].updatePosition(position);
        }

        for (let i = 0; i < this.MaxDigit; i++) {
            let digit = i + 1;
            if (!this.isShowDigit(digit, value))
                continue;

            let view = this.numbers[i];
            view.show();
            view.setNumber(this.getNumber(digit, value));
        }
    }

    public set tint(value: number) {
        for (let i = 0; i < this.MaxDigit; i++) {
            let view = this.numbers[i];
            view.tint = value;
        }
    }
    
    public clearNumbers() {
        for (let view of this.numbers) {
            view.hide();
        }
    }


    private isShowDigit(digit: number, score: number): boolean {
        return digit == 1 ? true :
            score >= Math.pow(10, (digit - 1));
    }

    private getNumber(digit: number, score: number): number {
        if (digit == 1)
            return score % 10;
        let pow = Math.pow(10, (digit - 1));
        let mod = score % pow;
        return ((score - mod) / pow) % 10;
    }

    private getWidth(score: number) {
        let digit = 0;
        for (let i = 1; i <= this.MaxDigit; i++) {
            if (!this.isShowDigit(i, score))
                break;

            digit++;
        }

        return digit * this.NumberWidth;
    }
}