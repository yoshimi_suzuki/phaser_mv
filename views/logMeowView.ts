﻿class LogMeowView extends View {
    public static attach(
        position:Atagoal.Position = new Atagoal.Position()){
        let view = new LogMeowView(position);
        return view;
    }

    private _message:Phaser.Text = null;

    public show(message:string) {
        if (this._message == null) {
            this.setImage(MV.ImageTags.White)
            this.image.scale.x = 400 / this.getImageSize(MV.ImageTags.White).x * ResolutionManager.instance.scale;
            this.image.scale.y = 26 / this.getImageSize(MV.ImageTags.White).x * ResolutionManager.instance.scale;
            this.image.tint = 0x0;
            this.image.alpha = 0.6;

            this._message =  this.addText(
                message,
                new Atagoal.Position(),
                18,
                "#fff"
            );
            this._message.anchor.x = 
            this._message.anchor.y = 0.5;
        }
        this._message.text = message;

        return this;
    }
}