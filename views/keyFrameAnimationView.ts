﻿enum KeyFrameAnimationPositionOffsetType {
    None = 0,
    RotatedLeftTop = 1
}


class KeyFrameAnimationView extends View {
    protected _animations: Array<KeyFrameAnimationNode> = new Array<KeyFrameAnimationNode>();

    protected _onFinished: Phaser.Signal = new Phaser.Signal();
    public set onFinished(callback: () => void) {
        this._onFinished.add(callback);
    }

    public setAnimation(
        animation: KeyFrameAnimation,
        phaserImage:Phaser.Image = this.image) {
        let node = this.getNode(phaserImage);
        let animationNode = new KeyFrameAnimationNode(
            animation,
            phaserImage
        );

        if (node != null) {
            this._animations[this.getIndex(phaserImage)] = animationNode;
        } else {
            this._animations.push(animationNode);
        }
        this.updateAnimation(animationNode);

        return this;
    }

    public tick() {
        for(let animationNode of this._animations) {
            this.updateAnimation(animationNode);
        }
    }


    public setPositionOffsetType(
        type: KeyFrameAnimationPositionOffsetType,
        phaserImage:Phaser.Image=  this.image
    ) {
        let node = this.getNode(phaserImage);
        if (node == null) return;
        node.positionOffsetType = type;
    }

    public setCenterPoint(
        width: number,
        height: number,
        centerX: number,
        centerY: number,
        phaserImage:Phaser.Image=  this.image
    ) {
        let node = this.getNode(phaserImage);
        if (node == null) return;
        node.width = width;
        node.height = height;
        node.centerX = centerX;
        node.centerY = centerY;
    }

    public setParentColor(
        r: number,
        g: number,
        b: number,
        a: number,
        phaserImage:Phaser.Image=  this.image
    ) {
        let node = this.getNode(phaserImage);
        if (node == null) return;
        node.parentColor.r = r;
        node.parentColor.g = g;
        node.parentColor.b = b;
        node.parentColor.a = a;
    }

    public getNode(phaserImage:Phaser.Image) {
        for(let node of this._animations)
            if (node.phaserImage == phaserImage)
                return node;
        return null;
    }

    public getIndex(phaserImage:Phaser.Image) {
        for (let i = 0; i < this._animations.length; i++) {
            if (this._animations[i].phaserImage == phaserImage)
                return i;
        }
        return -1;
    }

    protected updateAnimation(animationNode:KeyFrameAnimationNode)
    {
        let animation = animationNode.animation;

        if (animation.isFinished()) {
            return;
        }

        animation.tick();

        let image = animationNode.phaserImage;

        let defaultPosition = new Atagoal.Position(
            image.position.x,
            image.position.y
        );

        image.anchor.x = animationNode.centerX / animationNode.width;
        image.anchor.y = animationNode.centerY / animationNode.height;
        image.position.x = 0;
        image.position.y = 0;

        if (animation.rotation != null)
            image.rotation = Math.PI * animation.rotation / 180;

        if (animationNode.positionOffsetType == KeyFrameAnimationPositionOffsetType.RotatedLeftTop) {
            let leftTop = animation.position != null
                ? ResolutionManager.instance.convertPosition(animation.position)
                : defaultPosition;
            let vector = Atagoal.Vector.createFromPoints(
                new Atagoal.Position(
                    0, 0
                ),
                new Atagoal.Position(
                    animationNode.centerX, animationNode.centerY
                )
            );
            let rotatedVec = animation.rotation
                ? vector.getRotated(animation.rotation)
                : vector;
            let postPosition = ResolutionManager.instance.convertPosition(
                leftTop
                    .getMovedPosition(vector)
                    .getMoved(
                    (rotatedVec.x - vector.x),
                    (rotatedVec.y - vector.y)
                ));
            image.position.x = postPosition.x;
            image.position.y = postPosition.y;
        } else {
            let postPosition = animation.position != null
                ? ResolutionManager.instance.convertPosition(animation.position)
                : defaultPosition;
            image.position.x = postPosition.x;
            image.position.y = postPosition.y;
        }


        let scale = animation.scale;
        if (scale != null) {
            image.scale.x = scale.x * ResolutionManager.instance.scale;
            image.scale.y = scale.y * ResolutionManager.instance.scale;
        }

        let colors = animation.color;
        if (colors != null) {
            image.tint =
                Math.floor(0x0000ff * colors.r * animationNode.parentColor.r) * 0x010000
                    + Math.floor(0x0000ff * colors.g * animationNode.parentColor.g) * 0x000100
                    + Math.floor(0x0000ff * colors.b * animationNode.parentColor.b);
            image.alpha = colors.a * animationNode.parentColor.a;
        }

        if (animation.isFinished()) {
            this._onFinished.dispatch();
        }
    }
}

class KeyFrameAnimationNode {
    constructor(
        public animation:KeyFrameAnimation,
        public phaserImage:Phaser.Image,
        public width:number = 1,
        public height:number = 1,
        public centerX:number = 0.5,
        public centerY:number = 0.5,
        public positionOffsetType: KeyFrameAnimationPositionOffsetType = KeyFrameAnimationPositionOffsetType.None,
        public parentColor:KeyFrameColor = new KeyFrameColor(1,1,1,1)
    ){}
}