﻿class ExpandableImageView extends ButtonView {
    public static attach(position:Atagoal.Position = new Atagoal.Position()) {
        let view = new ExpandableImageView(position);

        return view;
    }

    constructor(position: Atagoal.Position) {
        super(position);
    }

    private _alpha:number = 1;
    private _tint:number = 0xffffff;
    private _image9SliceTags:Image9SliceTags = null;
    private _imageMap:KVSMap<Phaser.Image> = new KVSMap<Phaser.Image>();

    public setImages(imageTags:Image9SliceTags) {
        this._image9SliceTags = imageTags;

        this.setImage(imageTags.centerTag);

        let center = this.image;
        let leftTop = this.addImage(
            imageTags.leftTopTag,
            imageTags.leftTopPosition.clone());
        let left = this.addImage(
            imageTags.leftTag,
            imageTags.leftPosition.clone());
        let leftBottom = this.addImage(
            imageTags.leftBottomTag,
            imageTags.leftBottomPosition.clone());
        let top = this.addImage(
            imageTags.topTag,
            imageTags.topPosition.clone());
        let bottom = this.addImage(
            imageTags.bottomTag,
            imageTags.bottomPosition.clone());
        let rightTop = this.addImage(
            imageTags.rightTopTag,
            imageTags.rightTopPosition.clone());
        let right = this.addImage(
            imageTags.rightTag,
            imageTags.rightPosition.clone());
        let rightBottom = this.addImage(
            imageTags.rightBottomTag,
            imageTags.rightBottomPosition.clone());

        this._imageMap.register(
            Image9SliceTags.Center,
            center
        );
        this._imageMap.register(
            Image9SliceTags.LeftTop,
            leftTop
        );
        this._imageMap.register(
            Image9SliceTags.Left,
            left
        );
        this._imageMap.register(
            Image9SliceTags.LeftBottom,
            leftBottom
        );
        this._imageMap.register(
            Image9SliceTags.Top,
            top
        );
        this._imageMap.register(
            Image9SliceTags.Bottom,
            bottom
        );
        this._imageMap.register(
            Image9SliceTags.RightTop,
            rightTop
        );
        this._imageMap.register(
            Image9SliceTags.Right,
            right
        );
        this._imageMap.register(
            Image9SliceTags.RightBottom,
            rightBottom
        );

        leftTop.anchor.x = 1;
        leftTop.anchor.y = 1;
        left.anchor.x = 1;
        left.anchor.y = 0.5;
        leftBottom.anchor.x = 1;
        leftBottom.anchor.y = 0;
        top.anchor.y = 1;
        center.anchor.y = 0.5;
        bottom.anchor.y = 0;
        rightTop.anchor.x = 0;
        rightTop.anchor.y = 1;
        right.anchor.x = 0;
        right.anchor.y = 0.5;
        rightBottom.anchor.x = 0;
        rightBottom.anchor.y = 0;


        let borderImageScaleRatio = imageTags.border / leftTop.width;
        let centerImageScaleRatioX = imageTags.centerWidth / (center.width / center.scale.x);
        let centerImageScaleRatioY = imageTags.centerHeight / (center.height / center.scale.y);

        leftTop.scale.x =
        leftTop.scale.y =
        leftBottom.scale.x =
        leftBottom.scale.y =
        rightTop.scale.x =
        rightTop.scale.y =
        rightBottom.scale.x =
        rightBottom.scale.y =
        left.scale.x =
        right.scale.x =
        top.scale.y =
        bottom.scale.y = borderImageScaleRatio;
        top.scale.x =
        center.scale.x =
        bottom.scale.x = centerImageScaleRatioX;
        left.scale.y =
        center.scale.y =
        right.scale.y = centerImageScaleRatioY;

        for (let image of this._imageMap.getValues()) {
            image.scale.x *= ResolutionManager.instance.scale;
            image.scale.y *= ResolutionManager.instance.scale;
        }
        
        this.node.inputEnabled = true;

        return this;
    }
    
    public setMask(mask:Phaser.Graphics) {
        for (let image of this._imageMap.getValues()) {
            image.mask = mask;
        }
    }


    private _onFrameClicked:EventHolder = new EventHolder(); 
    public set onFrameClicked(callback:()=>void) {
        if (this.button == null) {
            this.setButtonImage(MV.ImageTags.White);
            this.button.scale.x = this._image9SliceTags.width / 10;
            this.button.scale.y = this._image9SliceTags.height / 10;
            this.button.alpha = 0;
            this.onClicked = ()=>{
                this._onFrameClicked.dispatch();
            };
        }

        this._onFrameClicked.add(callback);
    }

    public get width() {
        return this._image9SliceTags.width;
    }
    public get height() {
        return this._image9SliceTags.height;
    }

    public set alpha(alpha:number) {
        for (let image of this._imageMap.getValues())
            image.alpha = alpha;
        this._alpha = alpha;
    }
    public get alpha(){ return this._alpha; }

    public set tint(tint:number) {
        for (let image of this._imageMap.getValues())
            image.tint = tint;
        this._tint = tint;
    }
    public get tint(){ return this._tint; }
}

class Image9SliceTags {
    public static readonly LeftTop:string = "LeftTop";
    public static readonly Left:string = "Left";
    public static readonly LeftBottom:string = "LeftBottom";
    public static readonly Top:string = "Top";
    public static readonly Center:string = "Center";
    public static readonly Bottom:string = "Bottom";
    public static readonly RightTop:string = "RightTop";
    public static readonly Right:string = "Right";
    public static readonly RightBottom:string = "RightBottom";

    private _width:number = 64;
    private _height:number = 64;
    private _border:number = 8;

    private _imageTagMap:KVSMap<string> = MV.ImageTags.DefaultExpandableFrames;

    public setImages(imageTagMap:KVSMap<string>) {
        this._imageTagMap = imageTagMap;
        return this;
    }

    public setSize(
        width:number,
        height:number,
        border:number
    ) {
        this._width = width;
        this._height = height;
        this._border = border;

        return this;
    }

    public get leftTopTag() { return this._imageTagMap.get(Image9SliceTags.LeftTop); }
    public get leftTag() { return this._imageTagMap.get(Image9SliceTags.Left); }
    public get leftBottomTag() { return this._imageTagMap.get(Image9SliceTags.LeftBottom); }
    public get topTag() { return this._imageTagMap.get(Image9SliceTags.Top); }
    public get centerTag() { return this._imageTagMap.get(Image9SliceTags.Center); }
    public get bottomTag() { return this._imageTagMap.get(Image9SliceTags.Bottom); }
    public get rightTopTag() { return this._imageTagMap.get(Image9SliceTags.RightTop); }
    public get rightTag() { return this._imageTagMap.get(Image9SliceTags.Right); }
    public get rightBottomTag() { return this._imageTagMap.get(Image9SliceTags.RightBottom); }

    public get width() { return this._width; }
    public get height() { return this._height; }
    public get border() { return this._border; }
    public get centerWidth() { return this.width - this.border * 2; }
    public get centerHeight() { return this.height - this.border * 2; }

    public get imageTags() {
        return this._imageTagMap.getValues();
    }
    public static get ImageKeys() {
        return new Array<string>(
            Image9SliceTags.LeftTop,
            Image9SliceTags.Left,
            Image9SliceTags.LeftBottom,
            Image9SliceTags.Top,
            Image9SliceTags.Center,
            Image9SliceTags.Bottom,
            Image9SliceTags.RightTop,
            Image9SliceTags.Right,
            Image9SliceTags.RightBottom
        )
    }
    public get atlasNames() {
        let atlasNames = new Array<string>();
        for (let tag of this.imageTags) {
            let atlasName = ResourceManager.instance.getTextureAtlasName(tag);
            if (atlasNames.indexOf(atlasName) >= 0)
                continue;
                atlasNames.push(atlasName);
        }
        return atlasNames;
    }



    public get leftTopPosition() {
        return new Atagoal.Position(
            -0.5 * this.centerWidth,
            -0.5 * this.centerHeight
        );
    }
    public get leftPosition() {
        return new Atagoal.Position(
            -0.5 * this.centerWidth,
            0
        );
    }
    public get leftBottomPosition() {
        return new Atagoal.Position(
            -0.5 * this.centerWidth,
            0.5 * this.centerHeight
        );
    }
    public get topPosition() {
        return new Atagoal.Position(
            0,
            -0.5 * this.centerHeight
        );
    }
    public get centerPosition() {
        return new Atagoal.Position(
            0,
            0
        );
    }
    public get bottomPosition() {
        return new Atagoal.Position(
            0,
            0.5 * this.centerHeight
        );
    }
    public get rightTopPosition() {
        return new Atagoal.Position(
            0.5 * this.centerWidth,
            -0.5 * this.centerHeight
        );
    }
    public get rightPosition() {
        return new Atagoal.Position(
            0.5 * this.centerWidth,
            0
        );
    }
    public get rightBottomPosition() {
        return new Atagoal.Position(
            0.5 * this.centerWidth,
            0.5 * this.centerHeight
        );
    }

    public get positions() {
        let positions = new KVSMap<Atagoal.Position>();

        positions.register(Image9SliceTags.LeftTop, this.leftTopPosition);
        positions.register(Image9SliceTags.Left, this.leftPosition);
        positions.register(Image9SliceTags.LeftBottom, this.leftBottomPosition);
        positions.register(Image9SliceTags.Top, this.topPosition);
        positions.register(Image9SliceTags.Center, this.centerPosition);
        positions.register(Image9SliceTags.Bottom, this.bottomPosition);
        positions.register(Image9SliceTags.RightTop, this.rightTopPosition);
        positions.register(Image9SliceTags.Right, this.rightPosition);
        positions.register(Image9SliceTags.RightBottom, this.rightBottomPosition);

        return positions;
    }


    public get anchors() {
        let anchors = new KVSMap<Atagoal.Position>();

        anchors.register(Image9SliceTags.LeftTop, new Atagoal.Position(1, 1));
        anchors.register(Image9SliceTags.Left, new Atagoal.Position(1, 0.5));
        anchors.register(Image9SliceTags.LeftBottom, new Atagoal.Position(1, 0));
        anchors.register(Image9SliceTags.Top, new Atagoal.Position(0.5, 1));
        anchors.register(Image9SliceTags.Center, new Atagoal.Position(0.5, 0.5));
        anchors.register(Image9SliceTags.Bottom, new Atagoal.Position(0.5, 0));
        anchors.register(Image9SliceTags.RightTop, new Atagoal.Position(0, 1));
        anchors.register(Image9SliceTags.Right, new Atagoal.Position(0, 0.5));
        anchors.register(Image9SliceTags.RightBottom, new Atagoal.Position(0, 0));

        return anchors;
    }


    public get scales() {
        let scales = new KVSMap<Atagoal.Position>();

        let leftTopRaw = PhaserEngine.game.cache.getFrame(this._imageTagMap.get(Image9SliceTags.LeftTop));
        let centerRaw = PhaserEngine.game.cache.getFrame(this._imageTagMap.get(Image9SliceTags.Center));
        let BorderImageWidth = leftTopRaw.sourceSizeW;
        let CenterImageWidth = centerRaw.sourceSizeW;
        let CenterImageHeight = centerRaw.sourceSizeH;

        let borderImageScaleRatio = this.border / BorderImageWidth;
        let centerImageScaleRatioX = this.centerWidth / CenterImageWidth;
        let centerImageScaleRatioY = this.centerHeight / CenterImageHeight;

        let leftTop = new Atagoal.Position();
        let left = new Atagoal.Position();
        let leftBottom = new Atagoal.Position();
        let top = new Atagoal.Position();
        let center = new Atagoal.Position();
        let bottom = new Atagoal.Position();
        let rightTop = new Atagoal.Position();
        let right = new Atagoal.Position();
        let rightBottom = new Atagoal.Position();

        leftTop.x =
        leftTop.y =
        leftBottom.x =
        leftBottom.y =
        rightTop.x =
        rightTop.y =
        rightBottom.x =
        rightBottom.y =
        left.x =
        right.x =
        top.y =
        bottom.y = borderImageScaleRatio;
        top.x =
        center.x =
        bottom.x = centerImageScaleRatioX;
        left.y =
        center.y =
        right.y = centerImageScaleRatioY;

        scales.register(Image9SliceTags.LeftTop,leftTop);
        scales.register(Image9SliceTags.Left,left);
        scales.register(Image9SliceTags.LeftBottom,leftBottom);
        scales.register(Image9SliceTags.Top,top);
        scales.register(Image9SliceTags.Center,center);
        scales.register(Image9SliceTags.Bottom,bottom);
        scales.register(Image9SliceTags.RightTop,rightTop);
        scales.register(Image9SliceTags.Right,right);
        scales.register(Image9SliceTags.RightBottom,rightBottom);

        return scales;
    }
}

class ImageScaleRatio {
    constructor(
        public x,
        public y
    ){}
}