﻿class ButtonView extends View {
    protected button: Phaser.Button = null;

    protected _onClicked: Phaser.Signal = new Phaser.Signal();
    protected _onEnabled: Phaser.Signal = new Phaser.Signal();
    protected _onDisabled: Phaser.Signal = new Phaser.Signal();

    public set onClicked(callback: ()=>void) {
        this._onClicked.add(callback);
    }
    public set onEnabled(callback: ()=>void) {
        this._onEnabled.add(callback);
    }
    public set onDisabled(callback: ()=>void) {
        this._onDisabled.add(callback);
    }

    public clear() {
        this._onClicked = new Phaser.Signal();
        this._onEnabled = new Phaser.Signal();
        this._onDisabled = new Phaser.Signal();
    }

    public setButtonImage(imageName: string) {
        if (this.node == null) return;
        if (ResourceManager.instance.isTextureAtlasName(imageName)) {
            this.button = PhaserEngine.game.add.button(
                0, 0,
                ResourceManager.instance.getTextureAtlasName(imageName),
                () => {
                    if (this._onClicked != null)
                        this._onClicked.dispatch();
                });
            this.button.frameName = imageName;
        } else {
            this.button = PhaserEngine.game.add.button(
                0, 0,
                imageName,
                () => {
                    if (this._onClicked != null)
                        this._onClicked.dispatch();
                });
        }
        this.imageSizes.register(imageName, new Atagoal.Vector(this.button.width, this.button.height));
        this.button.anchor.x = 0.5;
        this.button.anchor.y = 0.5;
        this.button.scale.x = ResolutionManager.instance.scale;
        this.button.scale.y = ResolutionManager.instance.scale;
        this.node.addChild(this.button);
    }

    public enable() {
        if (this.button == null)
            return;
        this.button.inputEnabled = true;
        this.button.input.useHandCursor = true;
        this._onEnabled.dispatch();
    }

    public disable() {
        if (this.button == null)
            return;
        this.button.inputEnabled = false;
        this._onDisabled.dispatch();
    }

    public get enabled() {
        return this.button != null
            ? this.button.inputEnabled
            : false;
    }

    protected switchButtonImage(imageName: string) {
        if (this.button == null) return;
        if (ResourceManager.instance.isTextureAtlasName(imageName)) {
            this.button.frameName = imageName;
        } else {
            this.button.destroy();
            this.setButtonImage(imageName);
        }
    }
}