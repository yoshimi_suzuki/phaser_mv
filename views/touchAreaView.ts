﻿class TouchAreaView extends ButtonView {
    public static Attach(
        width:number,
        height:number,
        position: Atagoal.Position = new Atagoal.Position()
    ) {
        let view = new TouchAreaView(width,height,position);
        return view;
    }

    constructor(
        width:number,
        height:number,
        position: Atagoal.Position
    ) {
        super(position);
        this.setButtonImage(MV.ImageTags.White);
        this.button.scale.x = width / this.getImageSize(MV.ImageTags.White).x * ResolutionManager.instance.scale;
        this.button.scale.y = height / this.getImageSize(MV.ImageTags.White).y * ResolutionManager.instance.scale;
        this.button.alpha = 0;
    }

    public showArea() {
        this.button.alpha = 0.4;
        return this;
    }
}